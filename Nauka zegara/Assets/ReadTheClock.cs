﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReadTheClock : MonoBehaviour
{
    [SerializeField] GameObject PointerMinute;
    [SerializeField] GameObject PointerHour;

    int hour;
    int minute;
    void Start()
    {
        CreateTask();
    }

    int GetRandomNumber(int maxValue)
    {
        int randomNumber = Random.Range(0, maxValue);
        return randomNumber;
    }

    void SetRandomTime()
    {
        hour = GetRandomNumber(12);
        minute = GetRandomNumber(60);
    }

    void CreateTask()
    {
        SetRandomTime();
        DisplayTime();
        MovePointersAccordingToTheRandomTime();
    }
    void DisplayTime()
    {
        print($"{hour} : {minute}");
    }

    void MovePointersAccordingToTheRandomTime()
    {
        PointerHour.GetComponent<RectTransform>().eulerAngles = new Vector3(0f, 0f, -360f / 12f * hour - 360f/12f * minute/60f);
        PointerMinute.GetComponent<RectTransform>().eulerAngles = new Vector3(0f, 0f, -360f / 60f * minute);
    }
}
